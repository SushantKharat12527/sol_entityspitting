﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Entity_Spliting.Entity
{
    public class UserCommunication
    {
        public decimal UserId { get; set; }

        public String MobileNo { get; set; }

        public String EmailId { get; set; }
    }
}
